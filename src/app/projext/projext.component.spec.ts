import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjextComponent } from './projext.component';

describe('ProjextComponent', () => {
  let component: ProjextComponent;
  let fixture: ComponentFixture<ProjextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
